# import needed modules
import quandl
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

quandl.read_key()
selected = ['CNP', 'F', 'WMT', 'GE', 'TSLA']
#selected = ['TSLA', 'AAPL', 'GE']
data = quandl.get_table('WIKI/PRICES', ticker = selected,
                        qopts = { 'columns': ['date', 'ticker', 'adj_close'] },
                        date = { 'gte': '2014-1-1', 'lte': '2016-12-31' }, paginate=True)

# reorganise data pulled by setting date as index with
# columns of tickers and their corresponding adjusted prices
clean = data.set_index('date')
table = clean.pivot(columns='ticker').dropna()
days = table.shape[0]

# calculate daily and annual returns of the stocks
#pct_change = per cent change from one day to previous one
#probably is gonnabe used to calculate the volatility/coveriance
returns_daily = table.pct_change()
#returns_annual = returns_daily.mean() * 250
returns_total = returns_daily.mean() * days

#sem razão logica, só preguiça de renomear variáveis
returns_annual = returns_total

# get daily and covariance of returns of the stock
#cov_daily is a covariance matrix
#matriz de covariância é calculado pela library pandas
cov_daily = returns_daily.cov()
#cov_annual = cov_daily * 250
cov_total = cov_daily * days

#sem razão logica, só preguiça de renomear variáveis
cov_annual = cov_total

# empty lists to store returns, volatility and weights of imiginary portfolios
port_returns = []
port_volatility = []
stock_weights = []
sharpe_ratio = []

# set the number of combinations for imaginary portfolios
num_assets = len(selected)
num_portfolios = 1000

deviation = table-np.mean(table)
# populate the empty lists with each portfolios returns,risk and weights
for single_portfolio in range(num_portfolios):
    #generate random weights
    weights = np.random.random(num_assets)

    #normalize so sum of weights equals 1 === 100%
    weights /= np.sum(weights)
    returns = np.dot(weights, returns_annual)

    volatility = np.mean(np.abs(np.dot(deviation, weights)))

    port_returns.append(returns)
    port_volatility.append(volatility)
    stock_weights.append(weights)
    sharpe_ratio.append(returns/volatility)

# a dictionary for Returns and Risk values of each portfolio
portfolio = {'Returns': port_returns,
             'Volatility': port_volatility,
             'Ratio': sharpe_ratio}

# extend original dictionary to accomodate each ticker and weight in the portfolio
for counter,symbol in enumerate(selected):
    portfolio[symbol+' Weight'] = [Weight[counter] for Weight in stock_weights]

#data frame é uma classe da biblioteca pandas
df = pd.DataFrame(portfolio)

#muda colunas
column_order = ['Returns', 'Volatility','Ratio'] + [stock+' Weight' for stock in selected]

# reorder dataframe columns
df = df[column_order]


#Sharpe Ratio
index_sr_max = df['Ratio'].idxmax()
sr_max_row = df.iloc[index_sr_max]
sr_max = sr_max_row['Ratio']
vol_sr_max = sr_max_row['Volatility']
ret_sr_max = sr_max_row['Returns']

# plot the efficient frontier with a scatter plot
plt.style.use('seaborn')
df.plot.scatter(x='Volatility', y='Returns', figsize=(10, 8), grid=True, c=df['Ratio'])
plt.xlabel('Risk (MeanAbsoluteDeviation)')
plt.ylabel('Expected Returns')
plt.title('Efficient Frontier')
plt.scatter(vol_sr_max, ret_sr_max, c='blue')
plt.show()
