import numpy as np
'esse código iterativamente calcula pi usando numeros aleatórios, sendo uma aplicação trivial do método de monte carlo'

it = 1
x,y = np.random.rand(2)
acc = 0
while it < 100000000000:
    if (x**2 + y**2)**(0.5) < 1:
        acc += 1
    x,y = np.random.rand(2)
    print(it, ': ', 4*acc/it)
    it += 1
