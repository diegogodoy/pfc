#!/usr/bin/python


import numpy as np

def inv(m):
    return np.linalg.inv(m)

Pi =  np.array([0.0092,0.0032,0.0074,0.0059, 0.0064]) #Implied Equilibrium Excess Returns //market returns
S =  np.array([
[0.0113,0.0007,0.0073,0.0016, 0.0007],
[0.0007,0.0043,0.0012,0.0011, 0.0011],
[0.0073,0.0012,0.0202,0.0015, 0.0000],
[0.0016,0.0011,0.0015,0.0061, 0.0011],
[0.0007,0.0011,0.0000,0.0011, 0.0030]]) #CovMatrix


#View
Q =  np.array([0.01, 0.0175])
P =  np.array([[0,1,0,0,-1],[1,0,-1,0,0]])

omega = P@S@P.T
invOm = inv(omega)
invS = inv(S)

b = invS@Pi + P.T@invOm@Q
a = inv(invS + P.T@invOm@P)
#print(P.dot(S).dot(P.T))
#print(a) #correct
#print(b)
expectedExcessReturns=a@b
print(Pi)
print(expectedExcessReturns)

#Para validar se tem sentido, comparar Pi e expectedExcessReturns levando em consideração as Views
