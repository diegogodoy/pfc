# import needed modules
import quandl
import pandas as pd
from datetime import date
import quadprog as qp
import numpy as np
#below required by minimum mad
from scipy.optimize import minimize
from scipy.optimize import Bounds
#below required for microbenchmark
import time

def getData(selected, initialDate, endDate = date.today()):
    '''
    selected é uma lista da strings com nome das empresas
    initialDate e endDate são objetos do tipo datetime.date, de função obvia pelo nome
    '''
    quandl.read_key()
    data = quandl.get_table(
            'WIKI/PRICES',
            ticker = selected,
            qopts = { 'columns': ['date', 'ticker', 'adj_close'] },
            date = { 'gte': initialDate.isoformat(), 'lte': endDate.isoformat()}, paginate=True)

    clean = data.set_index('date')
    valueSecurity = clean.pivot(columns='ticker').dropna()
    days = valueSecurity.shape[0]
    #print('days: ', days)
    #print('valueSecurity: ', valueSecurity)

    #pct_change = per cent change from one day to previous one
    returns_daily = valueSecurity.pct_change() #o ganho percentual de um dia para outro
    returns_total = returns_daily.mean()*days #ganho percentual no periodo todo.
    #returns_annual = returns_daily.mean() * 250

    # get daily and covariance of returns of the stock
    #cov_daily is a covariance matrix
    #matriz de covariância é calculado pela library pandas
    cov_daily = returns_daily.cov()
    cov_total = cov_daily*days
    #cov_annual = cov_daily * 250
    return valueSecurity, cov_total, returns_total, days

def mad(returns, retornoMinimoDesejado):
    num_assets = len(returns.columns)

    #somatorio dos x_i deve ser igual a 1
    r1 = {'type':'eq', 'fun': lambda x: np.sum(x)-1 }

    #retorno deve ser maior que retornoMinimoDesejado:
    r2 = {'type':'ineq', 'fun': lambda x: np.dot(x, np.mean(returns))-retornoMinimoDesejado }

    r3 = [{'type':'ineq', 'fun': lambda x: 1-x[i]} for i in np.arange(0,num_assets)]

    # Computes the Mean Absolute Deviation for the current iteration of weights
    def fun(x, returns):
        return (returns - returns.mean()).dot(x).abs().mean()

    guess = np.ones(num_assets)#change guess, maybe use random
    #guess = np.random.rand(3)
    #guess = guess/np.sum(guess)

    #Constraints são ignorados, possivelmente limitação do solver
    cons = [r1, r2]
    #cons2 não é necessárioi pois bounds já cria essa restrição

    bounds = Bounds(np.zeros(num_assets), np.ones(num_assets))

    #apenas alguns methods aceitam bounds: TNC, SLSQP, F-BFGS-B
    #ordem de constraints é irrelevante
    var= minimize(fun, guess, args=returns, constraints=[r1,r2], bounds=bounds )
    print(var)
    return var

    #var = pd.Series(index=returns.columns, data=min_mad_results.x)
    #código usado como inspiração sofre do mesmo problema( x_i < 0), porém usando bounds para evitar isso, problema passa a ser insolúvel

def validateMadSolution():
    t0 = time.time()
    foo = mad(data.pct_change().dropna(), .05)
    t1 = time.time()
    #if(foo.success):
    x, retorno, risco = foo.x, np.dot(foo.x, np.mean(data.pct_change())), foo.fun
    print(retorno)
    print('Tempo de execução(s): ', np.round(t1-t0, decimals=3))
    print('X: ', x.round(decimals=3))
    print('\sum x_i: ', np.sum(x))
    print('Retorno: ', np.round((retorno-1)*100, decimals=3), '%')
    print('Risco: ', np.round(risco, decimals=3))
    #else:
    #    print("Otimização falhou, não foi possivel obter o resultado esperado")


sgl = ['TSLA', 'AAPL', 'GE']
#sgl = ["AAPL","ABBV","ABT","ACN" ,"AGN" ,"AIG" ,"ALL" ,"AMGN" ,
#        "AMZN" ,"AXP","BA","BAC","BIIB","BK","BLK","BMY","BRK.B",
#        "C","CAT","CELG","CHTR","CL","CMCSA","COF",
#        "COP","COST","CSCO","CVS","CVX","DHR","DIS","DUK","EMR","EXC","F",
#        "FB","FDX","FOX","FOXA",
#        "GD","GE","GILD","GM","GOOG","GS","HAL",
#        "HD","HON","IBM","INTC","JNJ","JPM","KHC","KMI","KO",
#        "LLY","LMT","LOW","MA","MCD","MDLZ","MDT","MET","MMM",
#        "MO","MON","MRK","MS","MSFT","NEE","NKE","ORCL","OXY","PCLN"]
data, cov, ret, days = getData(sgl, date(2014, 1, 1))

validateMadSolution()


#data.to_csv(path_or_buf="../quandlData/raw.csv", index=False, header=False)
#cov.to_csv(path_or_buf="../quandlData/CovMatrix.csv", index=False, header=False)
#ret.T.to_csv(path_or_buf="../quandlData/Return.csv", index=False, header=False)

#absoluteDeviation = data.copy()

#for indexColumn, column in enumerate(data):
    #get mean da coluna
#    columnMean = data[column].mean()
#    absoluteDeviation[column] = absoluteDeviation[column].apply(lambda x: abs(x-columnMean))
    #print('Column Mean: ', columnMean)
    #print(absoluteDeviation[indexColumn])

#print(absoluteDeviation)
#
#print(2*cov.values)

#g = 2*cov.values
#a = np.zeros([3])
#C representa a matriz de restrições, porém há apenas uma: somatorio de Xi = 1

#c=np.array(ret.values)
#print(ret)
#print(c)
#b = np.array([1])    #primeiro elemento representa que o somatório do valor investido não deve passar de 1. Que equivale a 100%
#print(b)
#print(g.shape)
#print(a.shape)
#print(c.shape)
#print(b.shape)
#sol = qp.solve_qp(
#        G = g,
#        a = a,
#        C = c.T,
#        b = b
#        )
'''
Solve a strictly convex quadratic program

Minimize     1/2 x^T G x - a^T x
Subject to   C.T x >= b

This routine uses the the Goldfarb/Idnani dual algorithm [1].

References
---------
... [1] D. Goldfarb and A. Idnani (1983). A numerically stable dual
    method for solving strictly convex quadratic programs.
    Mathematical Programming, 27, 1-33.

Parameters
----------
G : array, shape=(n, n)
    matrix appearing in the quadratic function to be minimized
a : array, shape=(n,)
    vector appearing in the quadratic function to be minimized
C : array, shape=(n, m)
    matrix defining the constraints under which we want to minimize the
    quadratic function
b : array, shape=(m), default=None
    vector defining the constraints
meq : int, default=0
    the first meq constraints are treated as equality constraints,
    all further as inequality constraints (defaults to 0).
factorized : bool, default=False
    If True, then we are passing :math:`R^{−1}` (where :math:`G = R^T R`)
    instead of the matrix G in the argument G.

Returns
-------
x : array, shape=(n,)
    vector containing the solution of the quadratic programming problem.
f : float
    the value of the quadratic function at the solution.
xu : array, shape=(n,)
    vector containing the unconstrained minimizer of the quadratic function
iterations : tuple
    2-tuple. the first component contains the number of iterations the
    algorithm needed, the second indicates how often constraints became
    inactive after becoming active first.
lagrangian : array, shape=(m,)
    vector with the Lagragian at the solution.
iact : array
    vector with the indices of the active constraints at the solution.


#print(np.zeros([3,3])'''
