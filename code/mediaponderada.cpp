#include <iostream>
#include <vector>
#include <stdexcept>
#include <iostream>
template<class T>
using list = std::initializer_list<T>;
using std::cout;
using std::endl;
using std::vector;

template<class Numeric>
constexpr auto weightedAverage(vector<Numeric> numbers, vector<Numeric> weight){
	if(numbers.size() != weight.size() && numbers.size() != 0)
		throw std::runtime_error("Size doesnt match");
	Numeric acumulator{},  weightAcumulator{};
	for(size_t i  = 0; i < numbers.size(); ++i){
		const auto& tmp = weight[i];
		weightAcumulator += tmp;
		acumulator += numbers[i] * weight[i];
	}
	return acumulator/weightAcumulator;
}

int main(){
	cout << weightedAverage<float>({1,2}, {1, 0.5}) << endl;
	return 0;
}
