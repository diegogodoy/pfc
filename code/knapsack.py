import math
import operator
import numpy as np
from scipy.optimize import linprog

#m = np.matrix([[1,2,3],[4,5,6]])

#solucao = linprog([1,1,1],[
#        [2,1,-1],
#        [1,1,3],
#        [2,1,3],
#        ], [10,20,60])


#esse solver minimiza por padrao
#funcao objetiva multiplicada por -1 para maximizar
solucaoproblema2 = linprog([-2,-3,-1],[
        [1, 1,1],
        [2,1,-1],
        [3,2,-1],
        ], [40,20,30])

def knapsack(custo, utilidade, maxCusto, bounds = None):
    #multiply por -1
    custo = np.multiply(-1, custo)
    if bounds is None:
        sol  = linprog(custo, utilidade, maxCusto)
    else:
        sol  = linprog(custo, utilidade, maxCusto, bounds = bounds)
    sol.fun *= -1
    return custo, utilidade, maxCusto, sol
#not real knapsack, pois aceita respostas inteiras
#nao ha muito testes e restricoes na linguagem
#easy to code, hard to find runtime errors

#print(knapsack([1,4,3], [1, 2, 3], 5))

def allAreInteger(toTestList):
    for x in toTestList:
        if not math.isclose(int(round(x)), x):
            return False
    return True

def getResiduo(lista):
    toReturn = [None]*len(lista)
    for i in range(0,len(lista)):
        upper = math.ceil(lista[i])
        lower = math.floor(lista[i])
        toReturn[i] = max(lista[i]-lower, upper-lista[i])
    return toReturn

def getIndexOfMax(lista):
    return max(enumerate(lista), key=operator.itemgetter(1))

def getDankVariant(lista):
    indexDank, valueDank = getIndexOfMax(getResiduo(lista));
    tmp = lista[indexDank]
    rightRestriction = math.ceil(tmp)
    leftRestriction = math.floor(tmp)
    return indexDank, valueDank, leftRestriction, rightRestriction


def knapsackInteiro(custo, utilidade, maxCusto, bounds = None):
    c,u,m,var = knapsack(custo, utilidade, maxCusto)

    if not var.success:
        return None

    if allAreInteger(var.x):
        return [var.x, var.fun]
    else:
        indexDank, valueDank, leftRestriction, rightRestriction = getDankVariant(var.x)

        #iniciando ramo esquerdo
        novaRestricao = [0]*len(utilidade[0])
        novaRestricao[indexDank] = 1

        leftUtilidade = utilidade + [novaRestricao]
        leftMax = m + [leftRestriction]

        leftResult =  knapsackInteiro(custo, leftUtilidade, leftMax)
        if leftResult is None:
            #inciando ramo direito
            lowerbound = [0] * len(utilidade[0])
            lowerbound[indexDank] = rightRestriction
            bounds = (lowerbound, None)
            return knapsackInteiro(custo, utilidade, maxCusto, bounds)
        else:
            return leftResult

lista = [1.3, 5.4, 6.5]
sol = knapsackInteiro([1,4,3], [[1, 2, 3]], [5])
print(sol)
#print(allAreInteger([1, 5, 6]))
#print(getResiduo(lista))
#print(getDankVariant(lista))

#print(solucaoproblema2)
