# import needed modules
import quandl
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time

from tabulate import tabulate
from datetime import date

quandl.read_key()
#endDate = date.today()
#deprecated
endDate = date(2018,4,11)
#initialDate = date(2013,1,1)
initialDate = date(2014,1,1)

selected = ['CNP', 'F', 'WMT', 'GE', 'TSLA', 'AAPL', 'MSFT']
#Número muito elevado de investimentos requer numero absurdo de portfolios gerados tornando a técnica inviável.
#selected = ["AAPL","ABBV","ABT","ACN" ,"AGN" ,"AIG" ,"ALL" ,"AMGN"]
        #"AMZN" ,"AXP","BA","BAC","BIIB","BK","BLK","BMY",
        #"C","CAT","CELG","CHTR","CL","CMCSA","COF",
        #"COP","COST","CSCO","CVS","CVX","DHR","DIS","DUK","EMR","EXC","F"]
        #"FB","FDX","FOX","FOXA"]
        #"GD","GE","GILD","GM","GOOG","GS","HAL",
        #"HD","HON","IBM","INTC","JNJ","JPM","KHC","KMI","KO",
        #"LLY","LMT","LOW","MA","MCD","MDLZ","MDT","MET","MMM",
        #"MO","MON","MRK","MS","MSFT","NEE","NKE","ORCL","OXY","PCLN"]

print('\nObtendo dados ----------------------------------------------------------------------')
#data = quandl.get_table('WIKI/PRICES', ticker = selected,
#                        qopts = { 'columns': ['date', 'ticker', 'adj_close'] },
#                        date = { 'gte': initialDate.isoformat(), 'lte': endDate.isoformat()}, paginate=True)
                        #date = { 'gte': '2014-1-1', 'lte': '2016-12-31' }, paginate=True)

# reorganise data pulled by setting date as index with
# columns of tickers and their corresponding adjusted prices
print('\nPreparando dados ----------------------------------------------------------------------')
#clean = data.set_index('date')
#table = clean.pivot(columns='ticker').dropna()

#table.to_csv(path_or_buf="quandlData.csv", index=False, header=False)
table = pd.read_csv("quandlData.csv")

days = table.shape[0]

# calculate daily and annual returns of the stocks
#pct_change = per cent change from one day to previous one
#probably is gonnabe used to calculate the volatility/coveriance
returns_daily = table.pct_change()
returns_total = returns_daily.mean() * days

returns_total = returns_daily.mean() * 250

# get daily returns and covariance of returns of the stock
#cov_daily is a covariance matrix

#matriz de covariância é calculado pela biblioteca pandas
cov_daily = returns_daily.cov()
cov_total = cov_daily * days
cov_total = cov_daily * 250

#disperdicei tempo na linha de baixo
#cov_header = cov_total.keys().levels[1].values
print('Matrix de covariância -------------------------------------------------------------------')
#se portfolio for muito grande o tabbulate não funciona
print(tabulate(cov_total, headers=selected, showindex=selected, tablefmt='psql'))

print()
v = np.array([selected,returns_total.values]).T
print("Retorno:")
print(tabulate(v, showindex=False, tablefmt='psql'))

# empty lists to store returns, volatility and weights of imaginary portfolios
port_returns = []
port_volatility = []
port_mad = []
stock_weights = []
sharpe_ratio = []

num_assets = len(selected)
num_portfolios = 100_000 #parametrizar isso e comparar
risk_free_rate = 0.05

deviation = table-np.mean(table)

print('\nGerando portfolios  -------------------------------------------------------------------')
t0 = time.time()
# populate the empty lists with each portfolios returns,risk and weights
for single_portfolio in range(num_portfolios):

    weights = np.random.random(num_assets)

    weights /= np.sum(weights)

    returns = np.dot(weights, returns_total)

    volatility = np.sqrt(np.dot(weights.T, np.dot(cov_total, weights)))

    mad = np.mean(np.abs(np.dot(deviation, weights)))

    port_returns.append(returns)
    port_volatility.append(volatility)
    stock_weights.append(weights)
    sharpe_ratio.append((returns-risk_free_rate)/volatility)
    port_mad.append(mad)

t1 = time.time()
print('Tempo de execução(s): ', np.round(t1-t0, decimals=3))

# a dictionary for Returns and Risk values of each portfolio
portfolio = {'Returns': port_returns,
             'Volatility': port_volatility,
             'Ratio': sharpe_ratio,
             'MAD':port_mad}

# extend original dictionary to accomodate each ticker and weight in the portfolio
for counter,symbol in enumerate(selected):
    portfolio[symbol+' Weight'] = [Weight[counter] for Weight in stock_weights]

#data frame é uma classe da biblioteca pandas
df = pd.DataFrame(portfolio)

#muda colunas
column_order = ['Returns', 'Volatility', 'Ratio', 'MAD'] + [stock+' Weight' for stock in selected]

# reorder dataframe columns
df = df[column_order]

#obter portfolio com menor volatilidade para um determinado retorno minimo
minimum_return = 0.15 #=15%
#https://is.gd/0Yv9FH
df_minimum_return = df[df['Returns']>=minimum_return]

if not df_minimum_return.empty:
    print('\n Melhor portfólio para retorno minimo especificado:')
    df_minimum_return.reset_index(drop=True, inplace=True)
    index2 = df_minimum_return['Volatility'].idxmin()
    row = df_minimum_return.iloc[index2]
    print(tabulate(np.array([row.keys().values, row.values]).T, showindex=False, tablefmt='psql'))


#Sharpe Ratio
index_sr_max = df['Ratio'].idxmax()
sr_max_row = df.iloc[index_sr_max]

sr_max = sr_max_row['Ratio']
vol_sr_max = sr_max_row['Volatility']
ret_sr_max = sr_max_row['Returns']

#sr_max = Sharpe Ratio Maximo
print('\n Melhor portfólio considerando a razão de Sharpe:')
print(tabulate(np.array([sr_max_row.keys().values, sr_max_row.values]).T, showindex=False, tablefmt='psql'))

#Menor Volatilidade
index_vol_min = df['Volatility'].idxmin()
vol_min_row = df.iloc[index_vol_min]
vol_min = vol_min_row['Volatility']
ret_vol_min = vol_min_row['Returns']
sr_vol_min = vol_min_row['Ratio']

print('\n Portfolio de menor risco')
print(tabulate(np.array([vol_min_row.keys().values, vol_min_row.values]).T, showindex=False, tablefmt='psql'))


#------------------------------------------------------------------------------
fig, (ax1,ax2) = plt.subplots(nrows=1, ncols=2)

df.plot.scatter(ax=ax1,
        x='Volatility',
        y='Returns',
        figsize=(10, 8),
        grid=True,
        c=df['Ratio'],
        s=.1)

df.plot.scatter(ax=ax2,
        x='MAD',
        y='Returns',
        figsize=(10, 8),
        grid=True,
        c=df['Ratio'],
        s=.1)

#plt.style.use('seaborn')

ax1.xaxis.set_label_text('Risco (Variância)')
ax1.yaxis.set_label_text('Retorno esperado')
ax1.set_title('MPT')
ax1.scatter(vol_sr_max, ret_sr_max, c='blue')
ax1.scatter(vol_min, ret_vol_min, c='red')

ax2.xaxis.set_label_text('Risco (Desvio Médio Absoluto)')
ax2.yaxis.set_label_text('Retorno esperado')
ax2.set_title('MAD')
ax2.scatter(sr_max_row['MAD'], ret_sr_max, c='blue')
ax2.scatter(vol_min_row['MAD'], ret_vol_min, c='red')

if not df_minimum_return.empty:
    ax1.scatter(row['Volatility'], row['Returns'], c='green')
    ax2.scatter(row['MAD'], row['Returns'], c='green')
plt.show()
#TODO separar codigo para fazer benchmark
