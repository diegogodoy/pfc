import numpy as np

#input var  isnt typed
def func(numbers):
    return sum(numbers)/max(len(numbers),1)

def weightedAverage(numbers, weights):
    #check if booth has the same size
    if len(numbers) != len(weights):
        raise Exception("Invalid Input, numbers & wight must be same size")
    acumulator = 0;
    for index, val in enumerate(numbers):
        acumulator += numbers[index]*weights[index]
    return acumulator/sum(weights)

#print(np.mean([1,2]))
#print(func([1,2]))
print(weightedAverage([1,2], [1,.5]))

#Efficient usa código antigo
